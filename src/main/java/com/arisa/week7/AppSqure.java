package com.arisa.week7;

public class AppSqure {
    public static void main(String[] args) {
        Square rect1 = new Square(10, 5);
        rect1.printSquareArea();
        rect1.printSquarePerimeter();
        Square rect2 = new Square(5, 3);
        rect2.printSquareArea();
        rect2.printSquarePerimeter();
    }
}
