package com.arisa.week7;

public class Circle {
    //Attributes
    private double radian;

    public Circle(int radian) { // Construtor
        this.radian = radian;
    }
    public int printCircleArea() {
        double area = 3.14 * (radian*radian);
        System.out.println("Calculate area" + " = " + area);
        return (int)area;
    } 
    public int printCirclePerimeter() {
        double perimeter = 2*3.14*radian ;
        System.out.println("Calculate perimeter" + " = " + perimeter); return (int)perimeter;
    }
}
