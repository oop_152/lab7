package com.arisa.week7;

public class Square {
    //Attributes
    private int height;
    private int width;

    public Square(int height, int width) {  //Construtor
        this.height = height;
        this.width = width;
    }
    public int printSquareArea() {
        int area = height * width;
        System.out.println("Calculate area" + " = " + area); return area;
    }
    public int printSquarePerimeter() {
        int perimeter = height + width;
        System.out.println("Calculate perimeter" + " = " + perimeter); return perimeter;
    }
}
