package com.arisa.week7;

public class Tree {
    private String name;
    private int x;
    private int y;

    public Tree(String name, int x, int y){
        this.name = name;
        this.x = x;
        this.y = y;
    }
    public void print(){
        System.out.println("Tree: "+ name + " X:"+ x +" Y:"+ y);
    }
    
}
