package com.arisa.week7;

public class CircleApp {
    public static void main(String[] args) {
        Circle circle1 = new Circle(1);
        circle1.printCircleArea();
        circle1.printCirclePerimeter();
        Circle circle2 = new Circle(2);
        circle2.printCircleArea();
        circle2.printCirclePerimeter();
    }
}
